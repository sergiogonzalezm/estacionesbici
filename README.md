# EstacionesBici

## Planteamiento

Basicamente consiste en realizar una aplicación cuyo fin es consultar los datos abiertos de MalagaBici (concretamente sobre el estacionamiento de las Bicis), y mostrar las estaciones de bicis que hay y las plazas libres y ocupadas, entre otros datos de interes. Al estar utilizando los datos de MalagaBici estos se actualizan automáticamente y la aplicación lo hace en consecuencia cada vez que se vuelven a cargar los datos.

La aplicación tiene un funcionamiento simple y básico de cara al usuario, permitiendo que este no necesite aprender a usarla.

## Funcionamiento

En el momento en que se arranca la aplicación, esta descarga en backend el CSV oficial de MalagaBici y lo lee, extrayendo de el la información de interés. Una vez hecho la muestra en pantalla poniendola a disposición del usuario.

Por temas de rendimiento se a utilizado un RecyclerView para mostrar la lista de estaciones y una única tabla en backend de estaciones para toda la aplicación.

## Tecnologias usadas.

- Programado en Android Studio.
- SKD de compilación: 27
- SDK mínima: 21
- Librerías de implementación:
  - Android HttpClient (usada para la descarga del CSV de MalagaBici)
  - Support RecyclerView (usada para mostrar la lista de estaciones con el mayor rendimiento)

