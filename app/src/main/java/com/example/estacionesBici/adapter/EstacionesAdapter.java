package com.example.estacionesBici.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.estacionesBici.R;
import com.example.estacionesBici.clasesObjetos.EstacionBici;

import java.util.List;

public class EstacionesAdapter extends RecyclerView.Adapter<EstacionesAdapter.ViewHolder> {

    private List<EstacionBici> estaciones;

    public EstacionesAdapter(List<EstacionBici> estaciones) {
        this.estaciones = estaciones;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvEstacion;

        public ViewHolder(View itemView) {
            super(itemView);

            this.tvEstacion = itemView.findViewById(R.id.tvEstacion);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.estacion_item_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvEstacion.setText(estaciones.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return estaciones.size();
    }

    public EstacionBici getItem(int index) {
        return estaciones.get(index);
    }
}

