package com.example.estacionesBici.iu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.estacionesBici.R;
import com.example.estacionesBici.adapter.EstacionesAdapter;
import com.example.estacionesBici.adapter.gestionOnClick.ClickListener;
import com.example.estacionesBici.adapter.gestionOnClick.RecyclerTouchListener;
import com.example.estacionesBici.application.ListaEstaciones;
import com.example.estacionesBici.conversor.ConversorCsv;
import com.example.estacionesBici.utilidades.RestClient;
import com.loopj.android.http.TextHttpResponseHandler;

public class ListaEstacionesActivity extends AppCompatActivity {

    private static final String permisoInternet = Manifest.permission.INTERNET;
    private static final int REQUEST_WRITE = 1;

    private static final String URL_BASE = "https://datosabiertos.malaga.eu/";
    private static final String URL_RELATIVA = "recursos/transporte/EMT/EMTocupestacbici/ocupestacbici.csv";

    private static final String AVISO_ERROR_ACTUALIZACION_LISTA = "No se pudo actualizar los datos.\n";

    private RecyclerView rvEstaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_estaciones);

        rvEstaciones = findViewById(R.id.rvEstaciones);

        if (!hayPermiso(permisoInternet)) {
            descargarEstacionesBici();
            inicializar();
        } else
            pedirPermiso(permisoInternet);
    }

    private void inicializar() {
        rvEstaciones.setAdapter(new EstacionesAdapter(ListaEstaciones.getLista()));
        rvEstaciones.setLayoutManager(new LinearLayoutManager(this));

        rvEstaciones.addOnItemTouchListener(new RecyclerTouchListener(this, rvEstaciones, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(ListaEstacionesActivity.this, DatosEstacionActivity.class);
                intent.putExtra("estacion", ((EstacionesAdapter)rvEstaciones.getAdapter()).getItem(position));
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void guardarListaEstaciones(String csv) {
        ListaEstaciones.guardarLista(ConversorCsv.toArrayList(csv));
    }

    private void descargarEstacionesBici() {
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.get(URL_BASE + URL_RELATIVA, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando");
                progreso.setCancelable(true);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                StringBuilder mensaje = new StringBuilder();

                progreso.dismiss();
                if (ListaEstaciones.listaGuardada())
                    mensaje.append(AVISO_ERROR_ACTUALIZACION_LISTA);
                mensaje.append(responseString);
                Toast.makeText(ListaEstacionesActivity.this, mensaje.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBytes, Throwable throwable) {
                StringBuilder mensaje = new StringBuilder();

                progreso.dismiss();
                if (ListaEstaciones.listaGuardada())
                    mensaje.append(AVISO_ERROR_ACTUALIZACION_LISTA);
                mensaje.append(throwable.getMessage());
                Toast.makeText(ListaEstacionesActivity.this, mensaje.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                guardarListaEstaciones(responseString);
                inicializar();
                progreso.dismiss();
            }
        });
    }

    private boolean hayPermiso(String permiso) {
        return ActivityCompat.checkSelfPermission(this, permiso) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void pedirPermiso(String permiso) {
        // pedir los permisos necesarios, porque no están concedidos
        ActivityCompat.requestPermissions(this, new String[] {permiso}, REQUEST_WRITE );
        // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_WRITE) {
            if (ActivityCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "No hay permisos suficientes", Toast.LENGTH_SHORT).show();
                this.finish();
            } else
                descargarEstacionesBici();
        }
    }
}
