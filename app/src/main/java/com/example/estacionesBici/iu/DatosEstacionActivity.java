package com.example.estacionesBici.iu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.estacionesBici.R;
import com.example.estacionesBici.clasesObjetos.EstacionBici;

public class DatosEstacionActivity extends AppCompatActivity {
    private static EstacionBici estacionActual;

    private TextView tvEstacionActual;
    private TextView tvDireccion;
    private TextView tvEstado;
    private TextView tvNumEstacionamientoLibre;
    private TextView tvNumEstacionamientoOcupado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_estacion);

        tvEstacionActual = findViewById(R.id.tvEstacionActual);
        tvDireccion = findViewById(R.id.tvDireccion);
        tvEstado = findViewById(R.id.tvEstado);
        tvNumEstacionamientoLibre = findViewById(R.id.tvNumEstacionamientoLibre);
        tvNumEstacionamientoOcupado = findViewById(R.id.tvNumEstacionamientoOcupado);

        Bundle extras = getIntent().getExtras();
        if (extras != null)
            estacionActual = (EstacionBici)extras.getSerializable("estacion");

        mostrarDatos();
    }

    private void mostrarDatos() {
        tvEstacionActual.setText(estacionActual.getNombre());
        tvDireccion.setText(estacionActual.getDireccion());
        tvEstado.setText(estacionActual.getEstado());
        tvNumEstacionamientoLibre.setText(Integer.toString(estacionActual.getNumLibres()));
        tvNumEstacionamientoOcupado.setText(Integer.toString(estacionActual.getNumOcupados()));
    }
}
