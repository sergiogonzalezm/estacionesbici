package com.example.estacionesBici.application;

import android.app.Application;

import com.example.estacionesBici.clasesObjetos.EstacionBici;

import java.util.ArrayList;

public class ListaEstaciones extends Application {
    private static ArrayList<EstacionBici> estacionesBici;

    public static ArrayList<EstacionBici> getLista() {
        return estacionesBici;
    }

    public static void guardarLista(ArrayList<EstacionBici> nuevaLista) {
        estacionesBici = nuevaLista;
    }

    public static boolean listaGuardada() {
        return estacionesBici != null;
    }
}
