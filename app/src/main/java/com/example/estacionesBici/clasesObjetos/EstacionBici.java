package com.example.estacionesBici.clasesObjetos;

import java.io.Serializable;

public class EstacionBici implements Serializable{
    private String nombre;
    private String direccion;
    private String estado;
    private int numLibres;
    private int numOcupados;

    public EstacionBici(String nombre, String direccion,String estado, int numLibres, int numOcupados) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.estado = estado;
        this.numLibres = numLibres;
        this.numOcupados = numOcupados;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEstado() {
        return estado;
    }

    public int getNumLibres() {
        return numLibres;
    }

    public int getNumOcupados() {
        return numOcupados;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setNumLibres(int numLibres) {
        this.numLibres = numLibres;
    }

    public void setNumOcupados(int numOcupados) {
        this.numOcupados = numOcupados;
    }

    public String toString() {
        return nombre + " - " + direccion;
    }
}
