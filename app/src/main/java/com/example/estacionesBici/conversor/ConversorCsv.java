package com.example.estacionesBici.conversor;

import com.example.estacionesBici.clasesObjetos.EstacionBici;

import java.util.ArrayList;

public class ConversorCsv {
    private static final Character CARACTER_SEPARADOR_CSV = ',';
    private static final Character CARACTER_MARCADOR_CSV = '"';
    private static final Character CARACTER_ENTER = '\n';

    private static final int POSICION_NOMBRE = 2;
    private static final int POSICION_DIRECCION = 4;
    private static final int POSICION_ESTADO = 6;
    private static final int POSICION_NUM_LIBRES = 8;
    private static final int POSICION_NUM_OCUPADOS = 9;



    private ConversorCsv() {}

    public static ArrayList<EstacionBici> toArrayList(String csv) {
        String nombre;
        String direccion;
        String estado;
        int numLibres;
        int numOcupados;

        ArrayList<String> datosCsv = toListaString(csv);
        ArrayList<EstacionBici> estacionesBici = new ArrayList<>();



        for (int i = 2; i < datosCsv.size(); i++) {
            nombre = getDato(datosCsv.get(i), POSICION_NOMBRE);
            direccion = getDato(datosCsv.get(i), POSICION_DIRECCION);
            estado = getDato(datosCsv.get(i), POSICION_ESTADO);
            numLibres = Integer.parseInt(getDato(datosCsv.get(i), POSICION_NUM_LIBRES));
            numOcupados = Integer.parseInt(getDato(datosCsv.get(i), POSICION_NUM_OCUPADOS));

            estacionesBici.add(new EstacionBici(nombre, direccion, estado, numLibres, numOcupados));
        }

        return estacionesBici;
    }

    private static String getDato(String linea, int numDato) {
        StringBuilder stringBuilder = new StringBuilder();
        int datosLeidos = 0;
        int contador;

        for (contador = 0; contador < linea.length(); contador++) {
            if (linea.charAt(contador) != CARACTER_SEPARADOR_CSV) {
                if (linea.charAt(contador) != CARACTER_MARCADOR_CSV)
                    stringBuilder.append(linea.charAt(contador));
            } else {
                datosLeidos++;
                if (datosLeidos == numDato)
                    return stringBuilder.toString().trim();
                else
                    stringBuilder.delete(0, stringBuilder.length());
            }
        }
        if (contador >= linea.length())
            return stringBuilder.toString().trim();
        return null;
    }

    private static ArrayList<String> toListaString(String csv) {
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> datosCsv = new ArrayList<>();

        for (int i = 0; i < csv.length(); i++) {
            if (csv.charAt(i) != CARACTER_ENTER)
                stringBuilder.append(csv.charAt(i));
            else {
                datosCsv.add(stringBuilder.toString());
                stringBuilder.delete(0, stringBuilder.length());
            }
        }
        return datosCsv;
    }
}
